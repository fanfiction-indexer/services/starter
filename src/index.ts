import { bootstrapHttpService } from '@fanfiction-indexer/spec';
import { LocalStarterService } from './service';

const service = new LocalStarterService();

export const entrypoint = bootstrapHttpService(service);
