import { Fandom, StarterService } from '@fanfiction-indexer/spec';

export class LocalStarterService implements StarterService {
    public startIndexing(fandom: Fandom): Promise<void> {
        console.log(`Starting ${Fandom[fandom]} indexing.`);
        return Promise.resolve();
    }
}
